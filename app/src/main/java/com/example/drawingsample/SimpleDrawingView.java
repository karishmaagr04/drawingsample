package com.example.drawingsample;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

public class SimpleDrawingView extends View {
    private Paint drawPaint;
    private float pointX;
    private float pointY;
    private float startX;
    private float startY;
    private float changedPointX;
    private float changedPointY;
    private OnCoordinateChange onCoordinateChange;

    public SimpleDrawingView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setFocusable(true);
        setFocusableInTouchMode(true);
        setupPaint();
    }

    private void setupPaint() {
        // Setup paint with color and stroke styles
        drawPaint = new Paint();
        drawPaint.setColor(getResources().getColor(R.color.colorPrimaryDark));
        drawPaint.setAntiAlias(true);
        drawPaint.setStrokeWidth(5);
        drawPaint.setStyle(Paint.Style.STROKE);
        drawPaint.setStrokeJoin(Paint.Join.ROUND);
        drawPaint.setStrokeCap(Paint.Cap.ROUND);
    }

    public void setOnCoordinateChangeListener(OnCoordinateChange onCoordinateChange){
        this.onCoordinateChange = onCoordinateChange;
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        pointX = event.getX();
        pointY = event.getY();
        changedPointX = pointX;
        changedPointY = pointY;
        // Checks for the event that occurs
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                startX = pointX;
                startY = pointY;
                return true;
            case MotionEvent.ACTION_MOVE:
                break;
            default:
                return false;
        }
        // Force a view to draw again
        postInvalidate();
        onCoordinateChange.setOnCoordinateChange();
        return true;
    }

    public float getPointX() {
        return pointX;
    }

    public float getPointY() {
        return pointY;
    }

    public float getStartX() {
        return startX;
    }

    public float getStartY() {
        return startY;
    }

    public float getChangedPointX() {
        return changedPointX;
    }

    public float getChangedPointY() {
        return changedPointY;
    }

    public void setChangedPointX(float changedPointX) {
        this.changedPointX = changedPointX;
    }

    public void setChangedPointY(float changedPointY) {
        this.changedPointY = changedPointY;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        canvas.drawRect(startX, startY, changedPointX, changedPointY, drawPaint);
    }

    interface OnCoordinateChange {
        void setOnCoordinateChange();
    }
}