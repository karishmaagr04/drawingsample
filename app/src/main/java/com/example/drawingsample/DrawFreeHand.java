package com.example.drawingsample;

import android.app.Activity;
import android.os.Bundle;

public class DrawFreeHand extends Activity {

    HandFreeView dv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        dv = new HandFreeView(this);
        setContentView(dv);
    }

}