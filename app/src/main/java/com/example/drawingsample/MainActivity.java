package com.example.drawingsample;

import android.os.Bundle;
import android.view.View;
import android.widget.SeekBar;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;

public class MainActivity extends AppCompatActivity implements SimpleDrawingView.OnCoordinateChange {

    private SeekBar seekBar;
    private AppCompatButton showCoordinate;
    private SimpleDrawingView simpleDrawingView;
    private TextView coordinateText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initializeView();
        setListeners();
    }

    private void setListeners() {
        simpleDrawingView.setOnCoordinateChangeListener(this);

        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                coordinateText.setText(null);
                simpleDrawingView.setChangedPointX(simpleDrawingView.getPointX() + progress * 5);
                simpleDrawingView.setChangedPointY(simpleDrawingView.getPointY() + progress * 5);
                simpleDrawingView.postInvalidate();
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                // for now we don't need it
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                // for now we don't need it
            }
        });

        showCoordinate.setOnClickListener(new View.OnClickListener() {
                                              @Override
                                              public void onClick(View v) {
                                                  coordinateText.setText(
                                                          getString(R.string.coordinates, Math.round(simpleDrawingView.getStartX()),
                                                                  Math.round(simpleDrawingView.getStartY()),
                                                                  Math.round(simpleDrawingView.getChangedPointX()),
                                                                  Math.round(simpleDrawingView.getChangedPointY())));
                                              }
                                          }
        );

    }

    private void initializeView() {
        seekBar = findViewById(R.id.seekbar);
        simpleDrawingView = findViewById(R.id.drawingView);
        showCoordinate = findViewById(R.id.showCoordinate);
        coordinateText = findViewById(R.id.cordinateView);
    }

    @Override
    public void setOnCoordinateChange() {
        coordinateText.setText(null);
    }
}